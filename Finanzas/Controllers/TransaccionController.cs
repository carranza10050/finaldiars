﻿using System;
using System.Collections.Generic;
using System.Linq;
using Finanzas.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace Finanzas.Controllers
{
    public class TransaccionController : Controller
    {
        private readonly ContextoFinanzas _context;
        public TransaccionController(ContextoFinanzas context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult Index(int id)
        {
            var transacciones = _context.Transaccions.
                Where(o => o.IdCuenta == id).ToList();
            ViewBag.Cuenta = _context.Cuentas.First(o => o.Id == id);
            return View("Index", transacciones);
        }

        [HttpGet]
        public ActionResult Crear(int id)
        {
            ViewBag.Tipos = new List<string> { "Ingreso", "Gasto" };
            ViewBag.CuentaId = id;
            return View();
        }

        [HttpPost]
        public ActionResult Crear(Transaccion transaccion)
        {
            var cuenta = _context.Cuentas.First(o => o.Id == transaccion.IdCuenta);

            if (transaccion.Tipo == "Gasto" && (cuenta.Limite + cuenta.Saldo) >= transaccion.Monto)
                transaccion.Monto *= -1;
            else if (transaccion.Tipo != "Ingreso")
            {
                TempData["Error"] = "Saldo de la cuenta superado";
                ModelState.AddModelError("Error", "Saldo de la cuenta superado");
            }

            if (ModelState.IsValid)
            {
                _context.Transaccions.Add(transaccion);
                _context.SaveChanges();
                ModificaMontoCuenta(transaccion.IdCuenta);
                return RedirectToAction("Index","Cuenta", new { id = transaccion.IdCuenta });
            }
            ViewBag.Tipos = new List<string> { "Ingreso", "Gasto" };
            ViewBag.CuentaId = transaccion.IdCuenta;
            return View(transaccion);
        }

        [HttpGet]
        public ActionResult Transferir()
        {
            var cuentas = _context.Cuentas
                .Where(o => o.IdUser == LoggedUser().Id)
                .ToList();
            return View(cuentas);
        }

        [HttpPost]
        public ActionResult Transferir(int CuentaID1, int CuentaID2, decimal amount)
        {
            var transaccion1 = new Transaccion
            {
                IdCuenta = CuentaID1,
                Fecha = DateTime.Now,
                Tipo = "Gasto",
                Descripcion = "Transferencia",
                Monto = amount * -1
            };
            var transaccion2 = new Transaccion
            {
                IdCuenta = CuentaID2,
                Fecha = DateTime.Now,
                Tipo = "Ingreso",
                Descripcion = "Transferencia",
                Monto = amount
            };

            var cuenta = _context.Cuentas.First(o => o.Id == transaccion1.IdCuenta);
            if ((cuenta.Limite + cuenta.Saldo) < amount)
            {
                TempData["Error"] = "Saldo de la cuenta superado";
                ModelState.AddModelError("Error", "Saldo de la cuenta superado");
            }

            if (ModelState.IsValid)
            {
                _context.Transaccions.Add(transaccion1);
                _context.Transaccions.Add(transaccion2);

                _context.SaveChanges();

                ModificaMontoCuenta(CuentaID1);
                ModificaMontoCuenta(CuentaID2);

                return RedirectToAction("Index", "Cuenta");
            }
            var cuentas = _context.Cuentas
                .Where(o => o.IdUser == LoggedUser().Id)
                .ToList();
            return View(cuentas);
        }

        protected User LoggedUser()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            return _context.Users.Where(o => o.Username == claim.Value).FirstOrDefault();
        }

        private void ModificaMontoCuenta(int cuentaId)
        {
            var cuenta = _context.Cuentas
                .Include(o => o.Transaccions)
                .FirstOrDefault(o => o.Id == cuentaId);

            var total = cuenta.Transaccions.Sum(o => o.Monto);
            cuenta.Saldo = total;
            _context.SaveChanges();
        }
    }
}
