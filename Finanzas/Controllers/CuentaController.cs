﻿using System;
using System.Collections.Generic;
using System.Linq;
using Finanzas.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Finanzas.Controllers
{
    [Authorize]
    public class CuentaController : Controller
    {
        private readonly ContextoFinanzas context;
        public CuentaController(ContextoFinanzas context)
        {
            this.context = context;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var cuentas = context.Cuentas
                .Include(o => o.Categoria)
                .Where(o => o.IdUser == LoggedUser().Id)
                .ToList();
            ViewBag.Total = cuentas.Sum(o => o.Saldo);
            return View("Index", cuentas);
        }

        [HttpGet]
        public ActionResult Registrar()
        {
            ViewBag.Categorias = context.Categorias
                .ToList();
            return View("Registrar", new Cuenta());
        }

        [HttpPost]
        public ActionResult Registrar(Cuenta cuenta)
        {
            if (ModelState.IsValid)
            {
                if(cuenta.IdCategoria == 2)
                {
                    cuenta.Limite = cuenta.Saldo;
                    cuenta.Saldo = 0;
                }else if(cuenta.Saldo > 0)
                {
                    cuenta.Limite = 0;
                    cuenta.Transaccions = new List<Transaccion>
                    {
                        new Transaccion
                        {
                            Fecha = DateTime.Now,
                            Tipo = "Ingreso",
                            Monto = cuenta.Saldo,
                            Descripcion = "Monto Inicial"
                        }
                    };
                }
                cuenta.IdUser = LoggedUser().Id;
                context.Cuentas.Add(cuenta);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Categorias = context.Categorias
            .ToList();
            return View("Registrar", cuenta);
        }

        protected User LoggedUser()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            var user = context.Users.Where(o => o.Username == claim.Value).FirstOrDefault();
            return user;
        }
    }
}
